test_that("No changepoint", {
  set.seed(1)
  trajectory <- cumsum(c(rnorm(50000,mean=0,sd=2)))
  diffusionConstants <- slidingWindow(trajectory,60,1,0,algorithm="MSD",msdPercent=25)
  diffusionSD_MSD <- mean(sqrt(2*diffusionConstants),na.rm=TRUE)
  diffusionConstants <- slidingWindow(trajectory,60,1,0,algorithm="MLE",25)
  diffusionSD_MLE <- mean(sqrt(2*diffusionConstants),na.rm = TRUE)

  expect_equal(length(diffusionConstants),50000-60+1)
  expect_true(abs(diffusionSD_MSD - 2) < 0.2)
  expect_true(abs(diffusionSD_MLE - 2) < 0.01)
})



test_that("Single changepoint, very clear", {
  set.seed(1)
  n<-50000
  displacements <- c(rnorm(n,mean=0,sd=1),rnorm(n,mean=0,sd=10))
  localizationNoise <- rnorm(length(displacements),mean=0,sd=2)
  x <- cumsum(displacements) + localizationNoise

  diffusionConstants <- slidingWindow(x,60,1,0,algorithm="MSD",25)
  diffusionSD_MSD <- sqrt(2*diffusionConstants)
  diffusionConstants <- slidingWindow(x,60,1,0,algorithm="MLE",25)
  diffusionSD_MLE <- sqrt(2*diffusionConstants)

  expect_true((mean(diffusionSD_MSD,na.rm=TRUE)-6)<1)
  expect_true((mean(diffusionSD_MLE,na.rm=TRUE)-6)<1)
})

