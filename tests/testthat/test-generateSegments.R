test_that("basic test, values ok, number of segments ok", {
  set.seed(1)

  segmentSize <- 1000
  framesPerSecond <- 1
  trueD <- 1
  trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 0
  nDatasets <- 100
  useTrueNoise <- TRUE

  segments <- generateSegments(segmentSize=segmentSize,
                   trueSD=trueSD,
                   noiseSD=noiseSD,
                   framesPerSecond=framesPerSecond,
                   nDatasets=nDatasets,
                   useTrueNoise=useTrueNoise)

  expect_equal(length(unique(segments$file)),nDatasets)
  expect_true(abs(mean(unique(segments$mle))-trueD)<0.01)
  expect_true(abs(mean(unique(segments$cve))-trueD)<0.01)
  expect_true(abs(mean(unique(segments$mleBerglund))-trueD)<0.01)
  expect_true(all(unique(segments$mleApparent)-unique(segments$mle)==0))
  expect_true(all(as.numeric(table(segments$file))==segmentSize))
})

test_that("basic test, higher D, no noise", {
  set.seed(1)

  segmentSize <- 1000
  trueD <- 100
  framesPerSecond <- 1
  trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 0
  nDatasets <- 100
  useTrueNoise <- TRUE

  segments <- generateSegments(segmentSize=segmentSize,
                               trueSD=trueSD,
                               noiseSD=noiseSD,
                               framesPerSecond=framesPerSecond,
                               nDatasets=nDatasets,
                               useTrueNoise=useTrueNoise)

  expect_equal(length(unique(segments$file)),nDatasets)
  expect_true(abs(mean(unique(segments$mle))-trueD)<1)
  expect_true(abs(mean(unique(segments$cve))-trueD)<1)
  expect_true(abs(mean(unique(segments$mleBerglund))-trueD)<1)
  expect_true(all(unique(segments$mleApparent)-unique(segments$mle)==0))
  expect_true(all(as.numeric(table(segments$file))==segmentSize))

})


test_that("basic test, higher D, with noise", {
  set.seed(1)

  segmentSize <- 1000
  trueD <- 10
  framesPerSecond <- 1
  trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 5
  nDatasets <- 100
  useTrueNoise <- TRUE

  segments <- generateSegments(segmentSize=segmentSize,
                               trueSD=trueSD,
                               noiseSD=noiseSD,
                               framesPerSecond=framesPerSecond,
                               nDatasets=nDatasets,
                               useTrueNoise=useTrueNoise)

  expect_equal(length(unique(segments$file)),nDatasets)
  expect_true(abs(mean(unique(segments$mle))-trueD)<1)
  expect_true(abs(mean(unique(segments$cve))-trueD)<1)
  expect_true(abs(mean(unique(segments$mleBerglund))-trueD)<1)

  expect_true(abs(mean(unique(segments$yangNoiseSD))-noiseSD)<0.1)

  expect_true(abs(2*mean(unique(segments$mle))*(1/framesPerSecond)+2*mean(unique(segments$yangNoiseSD))^2-
               2*mean(unique(segments$mleApparent))*(1/framesPerSecond))<0.01)

  expect_true(abs(mean(unique(segments$yangNoiseSD))-5)<0.01)
})

test_that("basic test, higher D, with noise, 1/15 frames per second", {
  set.seed(1)

  segmentSize <- 1000
  trueD <- 10
  framesPerSecond <- 15
  trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 5
  nDatasets <- 100
  useTrueNoise <- TRUE

  segments <- generateSegments(segmentSize=segmentSize,
                               trueSD=trueSD,
                               noiseSD=noiseSD,
                               framesPerSecond=framesPerSecond,
                               nDatasets=nDatasets,
                               useTrueNoise=useTrueNoise)

  expect_equal(length(unique(segments$file)),nDatasets)
  expect_true(abs(mean(unique(segments$mle))-trueD)<1)
  expect_true(abs(mean(unique(segments$mleBerglund))-trueD)<1)
  expect_true(abs(mean(unique(segments$cve))-trueD)<2)

  expect_true(abs(mean(unique(segments$yangNoiseSD))-noiseSD)<0.1)
  expect_true(abs(mean(unique(segments$cveNoiseSD))-noiseSD)<0.1)
  expect_true(abs(mean(unique(segments$msdNoiseSD),na.rm=TRUE)-noiseSD)<0.5)

  expect_true(abs(2*mean(unique(segments$mle))*(1/framesPerSecond)+2*mean(unique(segments$yangNoiseSD))^2-
                    2*mean(unique(segments$mleApparent))*(1/framesPerSecond))<0.5)

  expect_true(abs(mean(unique(segments$yangNoiseSD))-5)<0.01)
})


test_that("test noise larger than diffusion constant", {
  skip("Takes too long")
  set.seed(3)

  segmentSize <- 100000
  framesPerSecond <- 2
  trueD <- 5
  trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 10
  nDatasets <- 300
  useTrueNoise <- TRUE

  segments <- generateSegments(segmentSize=segmentSize,
                               trueSD=trueSD,
                               noiseSD=noiseSD,
                               framesPerSecond=framesPerSecond,
                               nDatasets=nDatasets,
                               useTrueNoise=useTrueNoise,
                               mleOnly = TRUE)

  estimatedSD <- sd(unique(segments$mle))
  predictedSD <- sqrt(varianceOfD(trueD,noiseSD,1/framesPerSecond,segmentSize))

  expect_equal(length(unique(segments$file)),nDatasets)
  expect_true(abs(mean(unique(segments$mle))-trueD)<0.1)
  expect_true(abs(mean(unique(segments$cve))-trueD)<0.1)

  expect_true(abs(estimatedSD-predictedSD)<1)

})



test_that("test generation using transition Matrix", {
  set.seed(3)

  transitionMatrix <- matrix(c(79.9,0.1,0.1,19.9),nrow=2, ncol=2, byrow = TRUE)
  colnames(transitionMatrix) <- c(100,10000)
  rownames(transitionMatrix) <- c(100,10000)

  segmentSize <- 1000
  framesPerSecond <- 2
  #trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 15

  data <- createSimulationFromTransitionMatrix(noiseSD_nm=noiseSD,
                                       framesPerSecond=framesPerSecond,
                                       nTimepoints=segmentSize,
                                       transitionMatrix=transitionMatrix,
                                       minimumLength = 5)

  #ggplot(data,aes(x=timepoint,y=xNm,colour=factor(trueD)))+geom_point()

  expect_equal(length(unique(data$trueD)),2)
})


test_that("test generation using transition Matrix", {
  set.seed(3)

  transitionMatrix <- matrix(c(79.9,0.1,0.1,19.9),nrow=2, ncol=2, byrow = TRUE)
  colnames(transitionMatrix) <- c(100,10000)
  rownames(transitionMatrix) <- c(100,10000)

  segmentSize <- 1000
  framesPerSecond <- 2
  #trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 0

  data <- createSimulationFromTransitionMatrix(noiseSD_nm=noiseSD,
                                               framesPerSecond=framesPerSecond,
                                               nTimepoints=segmentSize,
                                               transitionMatrix=transitionMatrix,
                                               minimumLength = 5)

  #ggplot(data,aes(x=timepoint,y=xNm,colour=factor(trueD)))+geom_point()
  expect_true(all((cumsum(data$displacement)-data$xNm) < 10^-10))
  expect_equal(length(unique(data$trueD)),2)
})


test_that("test generation using transition Matrix, multitude of switching", {
  set.seed(1)

  transitionMatrix <- matrix(c(49,1,1,49),nrow=2, ncol=2, byrow = TRUE)
  colnames(transitionMatrix) <- c(100,10000)
  rownames(transitionMatrix) <- c(100,10000)

  segmentSize <- 100000
  framesPerSecond <- 2
  #trueSD <- sqrt(trueD*2*(1/framesPerSecond))
  noiseSD <- 15

  data <- createSimulationFromTransitionMatrix(noiseSD_nm=noiseSD,
                                               framesPerSecond=framesPerSecond,
                                               nTimepoints=segmentSize,
                                               transitionMatrix=transitionMatrix,
                                               inference = FALSE,
                                               minimumLength = 5)

  #ggplot(data,aes(x=timepoint,y=xNm,colour=factor(trueD)))+geom_point()
  data$Type <- "Test"
  transitionMatrix <- createTransitionMatrix(dataset=data,
                                             type="Test",
                                             attribute="trueD",
                                             berglundHMM=FALSE,
                                             maxStateD=100000,
                                             transitionsOnly=FALSE)
  expect_true(transitionMatrix[1,1]<99)
  expect_true(transitionMatrix[1,1]>98)
  expect_true(transitionMatrix[1,2]<2)
  expect_true(transitionMatrix[1,2]>1)

})
