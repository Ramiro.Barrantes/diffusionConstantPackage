test_that("basic test for MLE Method", {
  set.seed(1)

  trueSD <- 1
  delta <- 1
  trueD <- (trueSD^2)/(2*delta)

  displacements <- rnorm(10000,mean=0,sd=trueSD)
  mleD <- calculateDeltaMLEXYWithNoise(displacements,1,0)
  mleSigma <- 2*mleD*1
  expect_true(abs(mleD-trueD)<0.1)

  cveD <- calculateDeltaCVE(displacements = displacements,delta = delta)
  expect_true(abs(cveD-trueD)<0.1)
  cveD <- calculateDeltaCVEWithNoise(displacements = displacements,delta = delta, noiseSD=0,R=0)
  expect_true(abs(cveD-trueD)<0.1)

  #calculateDeltaCVEExtra(displacements = displacements,delta = delta)

  msdD <- as.numeric(calculateDiffusionConstantAndIntercept(cumsum(displacements),1,25)$D)
  msdD_Version2 <- calc.D(cumsum(displacements),delta)
  msdD_Version3 <- calc.D.withNoise(cumsum(displacements),delta, 0,25)
  expect_equal(msdD,msdD_Version2)
  expect_equal(msdD,msdD_Version3)


  mleBerglund <- calculateMLEBerglund(displacements=displacements,
                              delta=delta,
                              noiseSD=0,
                              R=0)
  expect_true(abs(mleBerglund-trueD)<0.1)

})

test_that("basic test for MLE Method, other values", {
  set.seed(1)
  trueD <- 4
  displacements <- rnorm(10000,mean=0,sd=sqrt(2*trueD*1))
  mleD <- calculateDeltaMLEXYWithNoise(displacements,1,0)

  expect_true(abs(trueD-mleD)<0.1)

  msdD <- as.numeric(calculateDiffusionConstantAndIntercept(cumsum(displacements),1,25)$D)
  msdD_Version2 <- calc.D(cumsum(displacements),1)
  expect_equal(msdD,msdD_Version2)

  cveD <- calculateDeltaCVEWithNoise(displacements = displacements,delta = 1, 0,R=0)
  expect_true(abs(cveD-trueD)<0.1)

  mleBerglund <- calculateMLEBerglund(displacements=displacements,
                                      delta=1,
                                      noiseSD=0,
                                      R=0)
  expect_true(abs(mleBerglund-trueD)<0.1)

})

test_that("basic test for MSD Method", {
  trueSD <- 1
  delta <- 1
  trueD <- (trueSD^2)/(2*delta)

  set.seed(1)
  displacements <- rnorm(30000,mean=0,sd=trueSD)
  msdD <- calc.D(cumsum(displacements),1)
  msdSigma <- 2*msdD*1
  expect_equal(round(msdSigma,3),1.049)
})

test_that("calculate with noise", {
  noiseSD <- 3
  set.seed(1)
  displacements <- rnorm(1000000,mean=0,sd=1)
  trayectory <- cumsum(displacements)+rnorm(length(displacements),mean=0,sd=noiseSD)
  mleD <- calculateDeltaMLEXYWithNoise(diff(trayectory,1),1,noiseSD)
  noiseYang <- getNoiseVarianceYangMethod(trayectory,1)
  mleSigma <- 2*mleD*1
  expect_true(abs(mleSigma-1)<0.01)
  expect_true(abs(noiseSD-sqrt(noiseYang))<0.01)

  cveD <- calculateDeltaCVEWithNoise(displacements = diff(trayectory,1),delta = 1,noiseSD = 3,R=0)
  expect_true(abs(2*cveD-1)<0.1)
})

test_that("calculate MLE Berglund with noise", {
  displacements <- rnorm(100000,mean=0,sd=1)
  noiseSD <- 3
  trayectory <- cumsum(displacements)+rnorm(length(displacements),mean=0,sd=noiseSD)
  mleBerglund <- calculateMLEBerglundWithNoise(displacements=diff(trayectory,1),
                                      delta=1,
                                      R=0)
  mleSigma <- 2*mleBerglund["diffusionConstant"]*1
  mleNoise <- mleBerglund["noiseSD"]
  expect_true(abs(mleSigma-1)<0.05)
  expect_true(abs(mleNoise-3)<0.1)
})

test_that("calculate with a lot of noise", {
  trueSD <- 1
  delta <- 1/2
  trueD <- (trueSD^2)/(2*delta)

  noiseSD <- 30

  set.seed(1)
  displacements <- rnorm(100000,mean=0,sd=trueSD)
  trayectory <- cumsum(displacements)+rnorm(length(displacements),mean=0,sd=noiseSD)
  mleD <- calculateDeltaMLEXYWithNoise(diff(trayectory,1),delta,noiseSD)


  mleBerglund <- calculateMLEBerglund(displacements=diff(trayectory,1),
                                      delta=delta,
                                      noiseSD=noiseSD,
                                      R=0)
  expect_true(abs(mleBerglund-trueD)<0.1)


})



test_that("calculate diffusion constant with noise - MSD Method", {
  noiseSD <- 3
  set.seed(1)
  displacements <- rnorm(60000,mean=0,sd=1)
  trayectory <- cumsum(displacements)+rnorm(length(displacements),mean=0,sd=noiseSD)

  msdD <- calc.D.withNoise(trayectory,1,noiseSD,25)
  msdSigma <- 2*msdD*1
  expect_true(abs(msdSigma-1)<0.1)
})


test_that("calculate MLE Berglund with error", {
  skip("Warning on this one, not sure how to make it a test")
  trueSD <- 1
  delta <- 1/2
  trueD <- (trueSD^2)/(2*delta)

  noiseSD <- 30

  i<-330
  set.seed(i)
  displacements <- rnorm(60,mean=0,sd=trueSD)
  trayectory <- cumsum(displacements)+rnorm(length(displacements),mean=0,sd=noiseSD)
  mleBerglund <- calculateMLEBerglund(displacements=diff(trayectory,1),
                                      delta=delta,
                                      noiseSD=noiseSD,
                                      R=0)


})



